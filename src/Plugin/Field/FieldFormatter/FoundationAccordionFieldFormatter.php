<?php

namespace Drupal\foundation_accordion_field\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'foundation_accordion_field_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "foundation_accordion_field_formatter",
 *   label = @Translation("Accordion field formatter"),
 *   field_types = {
 *     "foundation_accordion_field_type"
 *   }
 * )
 */
class FoundationAccordionFieldFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      // Implement default settings.
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return array(
      // Implement settings form.
    ) + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }


  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $field_id = rand(0, 1000000);
    // print_r($items);
    foreach ($items as $delta => $item) {
      // print_r($items);
      // drupal_set_message($field_id);
      $field_id += 1;
      $title = $item->title;
      $body = $item->body;
      $format = $item->format;
      drupal_set_message($item->expanded);
      if ($item->expanded == 1){
        $expanded = "active";
      }
      else{
        $expanded = "";
      }

      $elements[$delta] = array(
        '#title' =>  $title,
        '#prefix' => '<ul class="accordion" data-accordion>',
        '#suffix' => '</ul>',
        '#body' => $body,
        '#format' => $format,
        '#expanded' => $expanded,
        '#acc_id' => $delta,
        '#field_id' => $field_id,
        '#theme' => 'foundation_accordion_field_formatter',
        '#attached' => array(
          'library' =>  array(
            'foundation_accordion_field/foundation-accordion-field'
          ),
        ),
      );
    }

    return $elements;
  }


  
  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  // protected function viewValue(FieldItemInterface $item) {
  //   // The text value has no text format assigned to it, so the user input
  //   // should equal the output, including newlines.
  //   return nl2br(Html::escape($item->title));
  //   // dpm($item);
  // }

}
