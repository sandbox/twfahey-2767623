<?php

namespace Drupal\foundation_accordion_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'foundation_accordion_field_widget' widget.
 *
 * @FieldWidget(
 *   id = "foundation_accordion_field_widget",
 *   label = @Translation("Accordion field widget"),
 *   field_types = {
 *     "foundation_accordion_field_type"
 *   }
 * )
 */
class FoundationAccordionFieldWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return array(
      'size' => 60,
      'placeholder' => '',
    ) + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = [];

    $elements['size'] = array(
      '#type' => 'number',
      '#title' => t('Size of textfield'),
      '#default_value' => $this->getSetting('size'),
      '#required' => TRUE,
      '#min' => 1,
    );
    $elements['placeholder'] = array(
      '#type' => 'textfield',
      '#title' => t('Placeholder'),
      '#default_value' => $this->getSetting('placeholder'),
      '#description' => t('Text that will be shown inside the field until a value is entered. This hint is usually a sample value or a brief description of the expected format.'),
    );

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $summary[] = t('Textfield size: !size', array('!size' => $this->getSetting('size')));
    if (!empty($this->getSetting('placeholder'))) {
      $summary[] = t('Placeholder: @placeholder', array('@placeholder' => $this->getSetting('placeholder')));
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];

    $element['title'] = $element + array(
      '#type' => 'textfield',
      '#title' => 'Title of accordion',

      '#default_value' => isset($items[$delta]->title) ? $items[$delta]->title : NULL,
      '#size' => $this->getSetting('size'),
      '#placeholder' => $this->getSetting('placeholder'),
      '#maxlength' => $this->getFieldSetting('max_length'),
    );
    $body_default = array(
      'value' => isset($items[$delta]->body) ? $items[$delta]->body : NULL,
      'format' => isset($items[$delta]->format) ? $items[$delta]->format : NULL
      );
    $element['body'] = array(
      '#type' => 'text_format',
      '#default_value' => isset($items[$delta]->body) ? $items[$delta]->body : NULL,
      '#format' => isset($items[$delta]->format) ? $items[$delta]->format : NULL,
      '#size' => $this->getSetting('size'),
      '#title' => 'Body for Accordion item',
      '#placeholder' => $this->getSetting('placeholder'),

      '#maxlength' => $this->getFieldSetting('max_length'),

      );

    $element['expanded'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($items[$delta]->expanded) ? $items[$delta]->expanded : NULL,
      '#size' => $this->getSetting('size'),

      '#placeholder' => $this->getSetting('placeholder'),

      '#maxlength' => $this->getFieldSetting('max_length'),
      '#title' => 'Expanded',


      );

    return $element;
  }


  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
      // print_r($values);

    foreach ($values as $delta => &$value) {

      $values[$delta]['format'] = $value['body']['format'];
      $values[$delta]['body'] = $value['body']['value'];
       
    }
    print_r($values);

    return $values;


  }

}
